<?php

namespace Maqe\MaqeMedia\Tests\Feature;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Maqe\MaqeMedia\Commands\GenerateStarterKitsCommand;
use Maqe\MaqeMedia\Tests\TestCase;

class GenerateStarterKitsCommandTest extends TestCase
{
    private $commandInstance;

    private $files;

    public function setUp(): void
    {
        parent::setUp();

        $this->commandInstance = new GenerateStarterKitsCommand;
        $this->files = [
            app_path('Enums/AbstractEnum.php'),
            app_path('Enums/MaqeMediaCollectionEnum.php'),
            app_path('Enums/MaqeMediaSizeEnum.php'),
            app_path('Enums/MaqeMediaVisibilityEnum.php'),
            app_path('Domain/MaqeMedia/Customs/PathGenerator.php'),
            app_path('Domain/MaqeMedia/Customs/PathGeneratorTest.php'),
            app_path('Domain/MaqeMedia/Customs/UrlGenerator.php'),
            app_path('Domain/MaqeMedia/Customs/UrlGeneratorTest.php'),
            app_path('Domain/MaqeMedia/Models/File.php'),
            app_path('Domain/MaqeMedia/Models/Fileable.php'),
            app_path('Domain/MaqeMedia/Models/MaqeMedia.php'),
            app_path('Domain/MaqeMedia/Repositories/FileableRepo.php'),
            app_path('Domain/MaqeMedia/Repositories/FileRepo.php'),
            app_path('Domain/MaqeMedia/Repositories/MaqeMediaRepo.php'),
            app_path('Domain/MaqeMedia/Services/FileService.php'),
            app_path('Domain/MaqeMedia/Services/FileServiceTest.php'),
            app_path('Domain/MaqeMedia/Services/MaqeMediaService.php'),
            app_path('Domain/MaqeMedia/Services/MaqeMediaServiceTest.php'),
            app_path('Domain/MaqeMedia/Traits/Fileable.php'),
            app_path('Domain/MaqeMedia/Traits/FileSyncable.php'),
            app_path('Http/Resources/Api/MaqeMedia/MaqeMediaResourceTest.php'),
            app_path('Http/Resources/Api/MaqeMedia/MaqeMediaResource.php'),
            app_path('Http/Resources/Api/MaqeMedia/MaqeMediaCollectionResource.php'),
            app_path('Http/Resources/Api/MaqeMedia/FileResourceTest.php'),
            app_path('Http/Resources/Api/MaqeMedia/FileResource.php'),
            app_path('Http/Resources/Api/MaqeMedia/FileCollectionResource.php'),
            database_path('factories/Domain/MaqeMedia/FileFactory.php'),
            database_path('factories/Domain/MaqeMedia/MaqeMediaFactory.php'),
            database_path('migrations/create_fileables_table.php'),
            database_path('migrations/create_files_table.php'),
            database_path('migrations/create_media_table.php'),
            storage_path('test_files/media_mock/media.jpg'),
        ];
    }

    public function test_stub_path_property_is_string_after_init_stub_path_called()
    {
        $actual = $this->getStrictAccessProperty($this->commandInstance, 'stubPath');

        $this->assertIsString($actual);
    }

    public function test_ddd_paths_property_is_array_after_init_ddd_paths_called()
    {
        $actual = $this->getStrictAccessProperty($this->commandInstance, 'dddPaths');

        $this->assertIsArray($actual);
    }

    public function test_stubs_property_is_array_after_init_stubs_called()
    {
        $actual = $this->getStrictAccessProperty($this->commandInstance, 'stubs');

        $this->assertIsArray($actual);
    }

    public function test_prepare_destination_file_infos_has_corrected_key()
    {
        $response = $this->invokeMethod(
            $this->commandInstance,
            'getDestinationFileInfos'
        );

        $actual1 = head($response);
        $actual2 = last($response);

        $this->assertArrayHasKey('filePath', $actual1);
        $this->assertArrayHasKey('class', $actual1);

        $this->assertArrayHasKey('filePath', $actual2);
        $this->assertArrayHasKey('class', $actual2);
    }

    public function test_prepare_file_directory_contains_storage_path_directory_for_mock_file()
    {
        $actualMockFile = $this->invokeMethod(
            $this->commandInstance,
            'prepareFileDirectory',
            [
                'fileName' => 'media.jpg',
                'destinationInfo' => [
                    'folder' => 'storage',
                    'filePath' => 'test_files/media_mock/media.jpg',
                    'class' => '',
                ],
            ]
        );

        $this->assertStringContainsString('storage/', $actualMockFile);

        $actual = $this->invokeMethod(
            $this->commandInstance,
            'prepareFileDirectory',
            [
                'fileName' => 'MaqeMediaCollectionEnum.php',
                'destinationInfo' => [
                    'folder' => 'app',
                    'filePath' => 'Enums/MaqeMediaCollectionEnum.php',
                    'class' => 'App\\Enums\\MaqeMediaCollectionEnum.php',
                ],
            ]
        );

        $this->assertStringNotContainsString('storage/', $actual);
    }

    public function test_prepare_destination_file_path_add_date_prefix_for_migration_file()
    {
        $fileName = 'create_fileables_table.php';
        $filePath = 'database/migrations';
        $response = $this->invokeMethod(
            $this->commandInstance,
            'prepareDestinationFilePath',
            [
                'fileName' => $fileName,
                'filePath' => $filePath,
            ]
        );
        $dateTimeStr = Str::between($response, $filePath . '/', '_' . $fileName);

        $this->assertTrue(Carbon::hasFormat($dateTimeStr, 'Y_m_d_His'));
    }

    private function cleanUpAllFiles(): void
    {
        // make sure to clean all migrations files with auto date prefix
        $existMigrationFiles = glob(database_path('migrations/*.php'));

        foreach (array_merge($this->files, $existMigrationFiles) as $file) {
            if (File::exists($file)) {
                unlink($file);
            }
        }
    }

    public function test_create_all_files_successfully()
    {
        $this->cleanUpAllFiles();

        Artisan::call('maqe-media:starter-kits');

        foreach ($this->files as $file) {
            if (Str::contains($file, 'database/migrations')) {
                $existMigrationFiles = glob(database_path('migrations/*.php'));
                $existingFiles = collect($existMigrationFiles)->map(function ($f) use ($file) {
                    $dateTimeStr = Str::between($f, database_path('migrations/'), '_create');
                    return Str::replace($dateTimeStr . '_', '', $f);
                })->all();

                $this->assertContains($file, $existingFiles);
            } else {
                if (File::exists($file) === false) {
                    dd($file);
                }
                $this->assertTrue(File::exists($file));
            }
        }

        $this->cleanUpAllFiles();
    }
}
