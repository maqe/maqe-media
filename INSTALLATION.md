# BASE INSTALLATION

สำหรับ Starter Kits มันจะทำการโคลน Stub files ซึ่งประกอบไปด้วย DDD structure, Config file ของ media library ซึ่งเราได้ทำการแก้ไขเพื่อใช้กับ starter kit เรียบร้อยแล้ว 

ณ ปัจจุบันนี้เรายังใช้งาน private repository อยู่ดังนั้นกรุณาติดตั้ง package ตามวิธีด้านล่างนี้

1. เพิ่มโค้ดใน `composer.json` 
```json
{
    "repositories": {
        "local": {
            "type": "vcs",
            "url": "git@bitbucket.org:maqe/maqe-media.git"
        }
    }
}
```

> การ install ด้วย private repository ต้องไปเพิ่ม SSH keys ใน bitbucket ด้วยหากใช้ url สำหรับ SSH

2. รันคำสั่งเพื่อติดตั้งใน autoload

```sh
composer require maqe/maqe-media
```

3. อย่าลืม ติดตั้ง media library ในโปรเจ็คตัวเองด้วย ซึ่งโค้ดตัวนี้ require version 9 ขึ้นไป พร้อมกับ laravel vesion 8

```sh
composer require "spatie/laravel-medialibrary:^9.0.0"
```

4. Publish config ที่เราทำเอาไว้ซึ่ง modified มาจาก media library config file อีกที

```sh
php artisan vendor:publish --provider="Maqe\MaqeMedia\MaqeMediaServiceProvider" --tag="maqe-media-config"
```

5. รันคำสั่งเพื่อโคลน Maqe Media DDD starter kits

```sh
php artisan maqe-media:starter-kits
```

6. ทดสอบโค้ดที่โคลนเข้ามาว่าสามารถใช้กับโปรเจ็คเราได้ไหม หรือมีอะไรต้องแก้ไขเพิ่มเติมด้วย phpunit test

```sh
php artisan test
```

7. สร้าง ดาต้าเบสด้วย migration file

```sh
php artisan migrate
```

8. เมื่อเสร็จเรียบร้อยแล้วเราก็สามารถแก้ไขปรับเปลี่ยน logic หรือ ผูก relation ได้ตามความต้องการของโปรเจ็คได้เลยครับ