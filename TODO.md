# MAQE MEDIA

เพื่อนๆ อย่าลืม อัพเดต `In Progress` กับ `Done` ด้วยนะครับ

> ตัวเลขวันที่ที่ตามหลังใน `Todo` เพื่อเอาไว้แทรกว่าไอเดียในการทำ todo คิดขึ้นมาเมื่อไหร่

## Todo

- [ ] Version 2 use Facade
  - [ ] ผู้ใช้ดาวโหลด package มาติดตั้ง
  - [ ] ผู้ใช้ migrate database และ config เข้าไปในโปรเจ็ค
  - [ ] ผู้ใช้ ผูก relation เข้าไปใส่ใน Model ของตัวเองได้ และจะได้ความสามารถของ trait ใน media lib ไปด้วย
  - [ ] ผู้ใช้ ผูก relation กลับเข้ามายัง package model ได้
  - [ ] ผู้ใช้ register media collection และ media conversion ได้
  - [ ] ผู้ใช้ ใช้ facade ในการ เพิ่ม ลบ ไฟล์​ แบบปรกติ และ ระบุ collection ได้
  - [ ] ผู้ใช้ ใช้ facade ในการเรียกดูไฟล์ทั้งหมดได้แบบ paginate


## In Progress

## Done ✓

- [x] สร้าง stub สำหรับ DDD structures - 2021-05-13
  - [x] สร้างในส่วนของ `app`, `database/migrations`, `database/factories`, `database/seeders` - 2021-05-13
  - [x] สร้างคำสั่งในการ ย้ายไฟล์ สำหรับทำการ upload file component และ register node package รวมถึง css และ js ใน laravel package - 2021-05-18
