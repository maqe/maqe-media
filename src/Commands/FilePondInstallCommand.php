<?php

namespace Maqe\MaqeMedia\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class FilePondInstallCommand extends Command
{
    protected $name = 'maqe-media:install-filepond';

    protected $description = 'Install the file pond component';

    private $fileManager;

    public function __construct()
    {
        parent::__construct();

        $this->fileManager = app(Filesystem::class);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // NPM Packages...
        $this->updateNodePackages(function ($packages) {
            return [
                'filepond' => '^4.27.1',
                'filepond-plugin-image-preview' => '^4.6.6',
                'filepond-plugin-image-resize' => '^2.0.10',
                'filepond-plugin-image-validate-size' => '^1.2.6',
            ] + $packages;
        });

        $destinationFilePath = resource_path('views/components/ui-kits/filepond.blade.php');
        $this->fileManager->ensureDirectoryExists(resource_path('views/components/ui-kits'));
        $this->fileManager->copy(__DIR__ . '/stubs/filepond/filepond.blade.php.stub', $destinationFilePath);

        $this->appendInFileIfNotExists(
            'import * as FilePond from \'filepond\';',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            'import FilePondPluginImagePreview from \'filepond-plugin-image-preview\';',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            'import FilePondPluginImageResize from \'filepond-plugin-image-resize\';',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            'import FilePondPluginImageValidateSize from \'filepond-plugin-image-validate-size\';',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            'window.FilePond = FilePond;',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            'window.FilePond.registerPlugin(FilePondPluginImagePreview);',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            'window.FilePond.registerPlugin(FilePondPluginImageResize);',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            'window.FilePond.registerPlugin(FilePondPluginImageValidateSize);',
            resource_path('js/bootstrap.js')
        );
        $this->appendInFileIfNotExists(
            '@import "filepond/dist/filepond.min.css";',
            resource_path('css/app.css')
        );
        $this->appendInFileIfNotExists(
            '@import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";',
            resource_path('css/app.css')
        );

        $this->line("<info>Created File:</info> {$destinationFilePath}");
        $this->info('File Pond Component installed successfully.');
        $this->comment('Please execute the "npm install && npm run dev" command to build your assets.');
    }

    /**
     * Update the "package.json" file.
     */
    protected static function updateNodePackages(callable $callback, bool $dev = true): void
    {
        if (! file_exists(base_path('package.json'))) {
            return;
        }

        $configurationKey = $dev ? 'devDependencies' : 'dependencies';

        $packages = json_decode(file_get_contents(base_path('package.json')), true);

        $packages[$configurationKey] = $callback(
            array_key_exists($configurationKey, $packages) ? $packages[$configurationKey] : [],
            $configurationKey
        );

        ksort($packages[$configurationKey]);

        file_put_contents(
            base_path('package.json'),
            json_encode($packages, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT) . PHP_EOL
        );
    }

    /**
     * append a given string within a given file,
     * when a given string does not exists.
     */
    protected function appendInFileIfNotExists(string $append, string $path): void
    {
        $fileContent = file_get_contents($path);
        $isExistInFileContent = Str::contains(strtolower($fileContent), strtolower($append));

        if ($isExistInFileContent === true) {
            return;
        }

        file_put_contents($path, PHP_EOL . $append, FILE_APPEND);
    }
}
