<?php

namespace App\Domain\MaqeMedia\Traits;

use Illuminate\Database\Eloquent\Model;

trait FileSyncable
{
    public function syncFiles(Model $model, array $fileIds, string $fileType): Model
    {
        if (empty($fileIds)) {
            $model->files()->wherePivot('type', $fileType)->detach();
        } else {
            $fileIds = collect($fileIds);
            $fileIds = $fileIds->mapWithKeys(function ($fileId) use ($fileType) {
                return [
                    $fileId => [
                        'type' => $fileType,
                    ],
                ];
            });

            // Workaround for sync() having bug when called multiple times (laravel issue 33072)
            $model->files()->wherePivot('type', $fileType)->detach();
            $model->files()->attach($fileIds);
        }

        return $model;
    }
}
