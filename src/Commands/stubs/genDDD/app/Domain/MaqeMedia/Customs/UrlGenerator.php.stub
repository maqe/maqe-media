<?php

namespace App\Domain\MaqeMedia\Customs;

use DateTimeInterface;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\Support\UrlGenerator\DefaultUrlGenerator;
use Spatie\MediaLibrary\Support\UrlGenerator\UrlGenerator as UrlGeneratorInterface;

class UrlGenerator extends DefaultUrlGenerator implements UrlGeneratorInterface
{
    public function getPath(): string
    {
        return $this->getPathRelativeToRoot();
    }

    public function getTemporaryUrl(DateTimeInterface $expiration, array $options = []): string
    {
        $disk = $this->media->disk;

        switch ($disk) {
            case 's3':
                return Storage::disk($disk)->temporaryUrl($this->getPath(), $expiration, $options);
            default:
                return $this->getUrl();
        }
    }

    public function getPathRelativeToRoot(): string
    {
        $fileName = $this->media->file_name;

        if (is_null($this->conversion)) {
            return $this->pathGenerator->getPath($this->media) . $fileName;
        }

        return $this->pathGenerator->getPathForConversions($this->media)
            . $this->conversion->getConversionFile($this->media);
    }
}
