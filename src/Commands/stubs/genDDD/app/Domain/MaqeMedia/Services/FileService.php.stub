<?php

namespace App\Domain\MaqeMedia\Services;

use App\Domain\MaqeMedia\Models\File;
use App\Domain\MaqeMedia\Repositories\FileableRepo;
use App\Domain\MaqeMedia\Repositories\FileRepo;
use App\Enums\MaqeMediaCollectionEnum;
use App\Enums\MaqeMediaVisibilityEnum;
use Illuminate\Pagination\LengthAwarePaginator;
use Spatie\Image\Image;

class FileService
{
    protected FileRepo $fileRepo;
    protected FileableRepo $fileableRepo;
    protected MaqeMediaService $maqeMediaService;

    public function __construct()
    {
        $this->fileRepo = new FileRepo();
        $this->fileableRepo = new FileableRepo();
        $this->maqeMediaService = new MaqeMediaService();
    }

    public function paginate(
        ?string $keyword,
        int $perPage,
        string $sortBy,
        string $sortDirection
    ): LengthAwarePaginator {
        return $this->fileRepo->paginate($keyword, $perPage, $sortBy, $sortDirection);
    }

    public function createFromRequest($keyName): File
    {
        $file = request()->file($keyName);
        $image = Image::load($file->path());
        $customProperties = [
            'width' => $image->getWidth(),
            'height' => $image->getHeight(),
        ];

        $model = $this->fileRepo->create([]);

        $this->maqeMediaService->addMediaFromRequest(
            $model,
            $keyName,
            MaqeMediaCollectionEnum::IMAGE,
            MaqeMediaVisibilityEnum::PUBLIC_READ,
            $customProperties
        );
        return $model;
    }

    public function createFromUrl(string $url): File
    {
        $image = Image::load($url);
        $customProperties = [
            'width' => $image->getWidth(),
            'height' => $image->getHeight(),
        ];

        $model = $this->fileRepo->create([]);
        $this->maqeMediaService->addMediaFromUrl(
            $model,
            $url,
            MaqeMediaCollectionEnum::IMAGE,
            MaqeMediaVisibilityEnum::PUBLIC_READ,
            $customProperties
        );

        return $model;
    }

    public function createFromFilePath($filePath): File
    {
        $image = Image::load($filePath);
        $customProperties = [
            'width' => $image->getWidth(),
            'height' => $image->getHeight(),
        ];

        $model = $this->fileRepo->create([]);
        $this->maqeMediaService->addMediaFromFilePath(
            $model,
            $filePath,
            MaqeMediaCollectionEnum::IMAGE,
            MaqeMediaVisibilityEnum::PUBLIC_READ,
            $customProperties
        );

        return $model;
    }

    public function delete(File $file): bool
    {
        // delete() will also trigger the media model that attached to it to be deleted
        // Also media library will automatically delete the file/folder for us
        return $file->delete() === true;
    }

    public function detachAllRelations(File $file): bool
    {
        return $this->fileableRepo->deleteByFileId($file->id);
    }

    public function getUrl(File $file, string $collection, string $conversion)
    {
        $url = '';
        $maqeMedia = $file->getFirstMedia($collection);

        if (! is_null($maqeMedia)) {
            $url = $maqeMedia->getFullUrl($conversion);
        }

        return $url;
    }
}
