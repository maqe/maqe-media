<?php

namespace App\Domain\MaqeMedia\Services;

use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\HasMedia;
use App\Domain\MaqeMedia\Models\MaqeMedia;
use App\Domain\MaqeMedia\Repositories\MaqeMediaRepo;

class MaqeMediaService
{
    protected MaqeMediaRepo $maqeMediaRepo;

    public function __construct()
    {
        $this->maqeMediaRepo = new MaqeMediaRepo();
    }

    public function find(int $id): MaqeMedia
    {
        return $this->maqeMediaRepo->find($id);
    }

    public function paginate(
        ?string $keyword,
        int $perPage,
        string $sortBy,
        string $sortDirection
    ) {
        $query = $this->maqeMediaRepo->model()->newQuery();

        if (! is_null($keyword)) {
            $query->where('name', 'like', '%' . $keyword . '%');
        }

        return $query
            ->orderBy($sortBy, $sortDirection)
            ->paginate($perPage);
    }

    public function addMediaFromRequest(
        HasMedia $model,
        string $keyName,
        string $collection,
        string $visibility,
        array $customProperties = []
    ): MaqeMedia {
        $file = request()->file($keyName);

        $customProperties = array_merge([
            'custom_headers' => [
                'ACL' => $visibility,
            ],
        ], $customProperties);

        return $model->addMediaFromRequest($keyName)
            ->preservingOriginal()
            ->withCustomProperties($customProperties)
            ->toMediaCollection($collection);
    }

    public function addMediaFromUrl(
        HasMedia $model,
        string $url,
        string $collection,
        string $visibility,
        array $customProperties = []
    ): MaqeMedia {
        $customProperties = array_merge([
            'custom_headers' => [
                'ACL' => $visibility,
            ],
        ], $customProperties);

        return $model->addMediaFromUrl($url)
            ->preservingOriginal()
            ->withCustomProperties($customProperties)
            ->toMediaCollection($collection);
    }

    public function addMediaFromFilePath(
        HasMedia $model,
        string $filePath,
        string $collection,
        string $visibility,
        array $customProperties = []
    ): MaqeMedia {
        $customProperties = array_merge([
            'custom_headers' => [
                'ACL' => $visibility,
            ],
        ], $customProperties);

        return $model->addMedia($filePath)
            ->preservingOriginal()
            ->withCustomProperties($customProperties)
            ->toMediaCollection($collection);
    }

    public function getContent(MaqeMedia $media): string
    {
        return Storage::disk($media->disk)->get($media->getPath());
    }
}
