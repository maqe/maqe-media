<?php

namespace Maqe\MaqeMedia\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class GenerateStarterKitsCommand extends Command
{
    protected $name = 'maqe-media:starter-kits';

    protected $description = 'Generate a maqe media DDD folder structure + files as a starter kits into the project';

    private $fileManager;

    private $stubPath;

    private $dddPaths;

    private $stubs;

    public function __construct()
    {
        parent::__construct();

        $this->fileManager = app(Filesystem::class);

        $this->initStubPath();

        $this->initDddPaths();

        $this->initStubs();
    }

    public function handle(): void
    {
        $this->info('Running Maqe Media DDD structure Generator...');
        $filesResponse = $this->getDestinationFileInfos();

        foreach ($filesResponse as $stubPath => $destinationInfo) {
            if ($this->laravel->runningInConsole()) {
                if (! class_exists($destinationInfo['class'])) {
                    $this->createFilesFromStub($stubPath, $destinationInfo);
                } else {
                    $this->askToCreateFilesFromStub($stubPath, $destinationInfo);
                }
            }
        }

        $this->line('');
        $this->comment('All done, Thank guys!');
    }

    private function initStubPath(): void
    {
        $this->stubPath = __DIR__ . '/stubs/genDDD';

        return;
    }

    private function initDddPaths(): void
    {
        $this->dddPaths = [
            'enums' => 'app/Enums',
            'customs' => 'app/Domain/MaqeMedia/Customs',
            'models' => 'app/Domain/MaqeMedia/Models',
            'repositories' => 'app/Domain/MaqeMedia/Repositories',
            'services' => 'app/Domain/MaqeMedia/Services',
            'traits' => 'app/Domain/MaqeMedia/Traits',
            'resources' => 'app/Http/Resources/Api/MaqeMedia',
            'factories' => 'database/factories/Domain/MaqeMedia',
            'migrations' => 'database/migrations',
            'files' => 'test_files/media_mock',
        ];

        return;
    }

    private function initStubs(): void
    {
        $this->stubs = [
            'app' => [
                $this->stubPath . '/' . $this->dddPaths['enums'] . '/AbstractEnum.php.stub',
                $this->stubPath . '/' . $this->dddPaths['enums'] . '/MaqeMediaCollectionEnum.php.stub',
                $this->stubPath . '/' . $this->dddPaths['enums'] . '/MaqeMediaSizeEnum.php.stub',
                $this->stubPath . '/' . $this->dddPaths['enums'] . '/MaqeMediaVisibilityEnum.php.stub',
                $this->stubPath . '/' . $this->dddPaths['customs'] . '/PathGenerator.php.stub',
                $this->stubPath . '/' . $this->dddPaths['customs'] . '/PathGeneratorTest.php.stub',
                $this->stubPath . '/' . $this->dddPaths['customs'] . '/UrlGenerator.php.stub',
                $this->stubPath . '/' . $this->dddPaths['customs'] . '/UrlGeneratorTest.php.stub',
                $this->stubPath . '/' . $this->dddPaths['models'] . '/File.php.stub',
                $this->stubPath . '/' . $this->dddPaths['models'] . '/Fileable.php.stub',
                $this->stubPath . '/' . $this->dddPaths['models'] . '/MaqeMedia.php.stub',
                $this->stubPath . '/' . $this->dddPaths['repositories'] . '/FileableRepo.php.stub',
                $this->stubPath . '/' . $this->dddPaths['repositories'] . '/FileRepo.php.stub',
                $this->stubPath . '/' . $this->dddPaths['repositories'] . '/MaqeMediaRepo.php.stub',
                $this->stubPath . '/' . $this->dddPaths['services'] . '/FileService.php.stub',
                $this->stubPath . '/' . $this->dddPaths['services'] . '/FileServiceTest.php.stub',
                $this->stubPath . '/' . $this->dddPaths['services'] . '/MaqeMediaService.php.stub',
                $this->stubPath . '/' . $this->dddPaths['services'] . '/MaqeMediaServiceTest.php.stub',
                $this->stubPath . '/' . $this->dddPaths['traits'] . '/Fileable.php.stub',
                $this->stubPath . '/' . $this->dddPaths['traits'] . '/FileSyncable.php.stub',
                $this->stubPath . '/' . $this->dddPaths['resources'] . '/MaqeMediaResourceTest.php.stub',
                $this->stubPath . '/' . $this->dddPaths['resources'] . '/MaqeMediaResource.php.stub',
                $this->stubPath . '/' . $this->dddPaths['resources'] . '/MaqeMediaCollectionResource.php.stub',
                $this->stubPath . '/' . $this->dddPaths['resources'] . '/FileResourceTest.php.stub',
                $this->stubPath . '/' . $this->dddPaths['resources'] . '/FileResource.php.stub',
                $this->stubPath . '/' . $this->dddPaths['resources'] . '/FileCollectionResource.php.stub',
            ],
            'database' => [
                $this->stubPath . '/' . $this->dddPaths['factories'] . '/FileFactory.php.stub',
                $this->stubPath . '/' . $this->dddPaths['factories'] . '/MaqeMediaFactory.php.stub',
                $this->stubPath . '/' . $this->dddPaths['migrations'] . '/create_fileables_table.php.stub',
                $this->stubPath . '/' . $this->dddPaths['migrations'] . '/create_files_table.php.stub',
                $this->stubPath . '/' . $this->dddPaths['migrations'] . '/create_media_table.php.stub',
            ],
            'storage' => [
                $this->stubPath . '/' . $this->dddPaths['files'] . '/media.jpg',
            ],
        ];

        return;
    }

    private function createFilesFromStub(string $stubPath, array $destinationInfo, bool $isOverWrite = false): void
    {
        try {
            $content = $content = $this->fileManager->get(
                $stubPath
            );

            $fileName = basename($destinationInfo['filePath']);
            $fileDirectory = $this->prepareFileDirectory($fileName, $destinationInfo);
            $destinationFilePath = $this->prepareDestinationFilePath($fileName, $fileDirectory);

            if (! $this->fileManager->exists($fileDirectory)) {
                $this->fileManager->makeDirectory($fileDirectory, 0755, true);
            }

            $this->fileManager->put($destinationFilePath, $content);

            if ($isOverWrite === true) {
                $this->line("<comment>Overwritten File:</comment> {$destinationFilePath}");
            } else {
                $this->line("<info>Created File:</info> {$destinationFilePath}");
            }
        } catch (\Exception $exception) {
            dd($exception);
            $this->line("<error>Fail to create file:</error> {$destinationFilePath} <error>with Error:</error> {$exception->getMessage()}");

            return;
        }
        return;
    }

    private function askToCreateFilesFromStub(string $stubPath, array $destinationInfo): void
    {
        $answer = $this->ask("File: {$destinationInfo['filePath']} already exist. Would you like to overwrite it?", 'Yes');

        if (! in_array(strtolower($answer), ['y', 'yes'])) {
            $this->line("<comment>Ignore Overwrite File:</comment> {$destinationInfo['filePath']}");
            $this->line('');

            return;
        }

        $this->createFilesFromStub($stubPath, $destinationInfo);

        return;
    }

    /**
     * Not check storage and migration file in this method
     */
    private function getDestinationFileInfos(): array
    {
        $destinationFilesResult = [];

        foreach ($this->stubs as $folder => $files) {
            foreach ($files as $stub) {
                $filePath = str_replace([$this->stubPath . '/', '.stub'], '', $stub);
                $class = str_replace('/', '\\', $filePath);
                $class = str_replace('.php', '', $class);
                $class = ucwords($class, '\\');
                $destinationFilesResult[$stub] = [
                    'folder' => $folder,
                    'filePath' => $filePath,
                    'class' => $class,
                ];
            }
        }

        return $destinationFilesResult;
    }

    private function prepareFileDirectory(string $fileName, array $destinationInfo): string
    {
        $filePath = Str::before($destinationInfo['filePath'], '/' . $fileName);

        if (Str::contains($destinationInfo['folder'], ['database'])) {
            return database_path(Str::after($filePath, 'database/'));
        }

        if (Str::contains($destinationInfo['folder'], 'storage')) {
            return storage_path(Str::after($filePath, 'storage/'));
        }

        // Default create in app folder
        return app_path(Str::after($filePath, 'app/'));
    }

    private function prepareDestinationFilePath(string $fileName, string $fileDestination): string
    {
        if (Str::contains($fileDestination, 'database/migrations')) {
            $datePrefix = date('Y_m_d_His');

            return $fileDestination . '/' . $datePrefix . '_' . $fileName;
        }

        return $fileDestination . '/' . $fileName;
    }
}
