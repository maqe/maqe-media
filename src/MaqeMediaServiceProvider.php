<?php

namespace Maqe\MaqeMedia;

use Illuminate\Support\ServiceProvider;
use Maqe\MaqeMedia\Commands\FilePondInstallCommand;
use Maqe\MaqeMedia\Commands\GenerateStarterKitsCommand;

class MaqeMediaServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerMergeConfig();
    }

    public function boot()
    {
        // Register the command if we are using the application via the CLI
        if ($this->app->runningInConsole()) {
            $this->registerCommands();
            $this->registerPublishConfig();
        }
    }

    private function registerCommands()
    {
        $this->commands([
            GenerateStarterKitsCommand::class,
            FilePondInstallCommand::class,
        ]);
    }

    // Test config only full fill test purpose
    // User should config filesystem in main project config/filesystems.php for their desire destination
    private function registerMergeConfig()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/filesystems.php', 'filesystems.disks');
    }

    private function registerPublishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/media-library.php' => config_path('media-library.php'),
        ], 'maqe-media-config');
    }
}
