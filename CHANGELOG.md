# Changelog

All notable changes to `maqe-media` will be documented in this file

## 1.0.0 - 2021-06-01
- initial release
