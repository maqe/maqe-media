# Maqe Media

Laravel Maqe Media ใช้เพื่อเพิ่มความรวดเร็วในการทำงานกับ Laravel Framework ซึ่งพวกเราใข้กันภายในบริษัท ซึ่ง package ตั้งใจทำเป็นตัว starter kits ที่ต่อยอดมาจาก `spatie/laravel-medialibrary` โดยทำใช้ร่วมกับการ ทำงานแบบ DDD และ Polymorphic tables.

## Required สำหรับโปรเจ็คที่จะนำ package นี้ไปใช้งาน

- Laravel v.8 ขึ้นไป
- Media Library v.9 ขึ้นไป
## Development Setup

วิธีการเดฟในเครื่อง local สามารถทำตามวิธีนี้ได้ครับ

1.  clone project มาที่เครื่องตัวเอง
2.  สร้าง laravel project ให้วางอยู่ในระกับเดียวกับ package ที่โคลนมา

```
\maqe-media
\laravel-project
```

3. ทำการ link local package เข้าไปใน `laravel-project`

```sh
# cd ไปที่ laravel-project
cd laravel-project

# link package เข้าไปใน composer.json
composer config repositories.local '{"type": "path", "url": "../maqe-media"}' --file composer.json

```

4. จากนั้นใช้คำสั่ง `composer require` เพื่อเพิ่ม package เข้าไปใน dependency โดยเราจะเอาไว้ที่ `require-dev`

```sh
composer require maqe/maqe-media --dev
```

5. เริ่มเดฟ ได้เลยครับ

## Base installation สำหรับโปรเจ็คที่เอาไปใช้งาน

เข้าไปดูรายละเอียดได้ [ที่นี่](INSTALLATION.md)

## Maqe Media Starter Kits

Generate a maqe media DDD folder structure + files as a starter kits into the project

คำสั่งจะทำการโคลนไฟล์ทั้งหมดไปที่โปรเจ็คเพื่อให้ผู้ใช้งานสามารถปรับแต่งแก้ไขได้ตามอัธยาศัย

```sh
php artisan maqe-media:starter-kits
```

## FilePond Component

คำสั่งสำหรับ Install  FilePond Anonymous component

คำสั่งจะสร้างไฟล์ blade ซึ่งเป็น component ไฟล์ที่เราจะเอาไปใช้งาน และจะทำการอัพเดต `package.json` รวมถึง register ในไฟล์ต่างที่จะเป็น ไม่ว่าจะเป็น `resources/app/bootstrap.js` และ `resources/css/app.css` ให้โดยอัตโนมัติ 

```sh
php artisan maqe-media:install-filepond 

# เมื่อไฟล์ถูกสร้าง และ register แล้วให้รันคำสั่ง
npm install && npm run dev 
```

ตัวอย่างการเอาไปใช้งานใน blade template

```html
<x-ui-kits.filepond 
    wire:model="brand.image" 
    allowImagePreview
    imagePreviewMaxHeight="400"
    allowFileTypeValidation
    acceptedFileTypes="['image/png', 'image/jpg', 'image/jpeg']"
    allowFileSizeValidation
    maxFileSize="4mb"    
/>
```

> filepond component ใช้ร่วมกับ `livewire` สำหรับการอัพโหลดไฟล์ อย่าลืมเพิ่ม `WithFileUploads` ด้วยนะครับ

```php

use Livewire\WithFileUploads;

class SomeLiveWireComponent extends Component
{
    use WithFileUploads;

    ...
}

```
## Contributing

ขอบคุณที่สนใจ ถ้าต้องการร่วมพัฒนาต่อยอดโปรเจ็คนี้ คุณสามารถเข้าไปอ่านคู่มือได้ [ที่นี่](CONTRIBUTING.md)

## TODO LIST

เข้าตรวจสอบและอัพเดต todo list ได้ [ที่นี่](TODO.md)

## CHANGELOG

เข้าตรวจสอบ changelog ได้ [ที่นี่](CHANGELOG.md)


## Issue ไม่สามารถใช้ private repo ใน Bitbucket ได้

- อย่าลืม เพิ่ม ssh public key ใน bitbucket account
- ถ้าเพิ่มไปแล้วยังไม่ได้อีก มันแสดง error 

```sh
  git@bitbucket.org: Permission denied (publickey).
  fatal: Could not read from remote repository.
```

- ให้ใจเย็นๆ แล้วลองรันคำสั่ง `ssh-add -l` ถ้ามันขึ้นว่า `The agent has no identities.`
  - สรุปคือเรายังไม่ได้ ssh-add ดังนั้นให้เราพิมพ์คำสั่ง `ssh-add ~/.ssh/<your_private_key>` แล้วลอง ใหม่ เท่านี้ก็จะหมดปัญหา `Permission denied (publickey)` แล้ว
