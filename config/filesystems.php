<?php

return [
    'test-media' => [
        'driver' => 'local',
        'root' => storage_path('test_files/media_mock'),
        'url' => public_path(),
    ],
];
